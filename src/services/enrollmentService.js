import axios from "axios";
import { API_URL } from "config.js/settings";
import ApiService from "./apiService";
import AuthService from "./authService";
const api = axios.create({
  baseURL: API_URL
})

class EnrollmentService {
  constructor() {
    api.interceptors.response.use(
      res => {
          return res;
      },
      err => {
        if(err.response.status == 401){
          AuthService.logoutUser();
        }
      }
    );
  }

  async getEnrollmentList(studentId){
    let params = new URLSearchParams();
    params = params.append('Student', studentId);

    return await api.get('enrollments', {headers: ApiService.httpHeaders(), params: params});
  }

  async getEnrollmentDetails(id){
    return await api.get(`enrollments/${id}`, { headers: ApiService.httpHeaders() } );
  }

  async insertEnrollment(enrollment){
    return await api.post(`enrollments`, enrollment, { headers: ApiService.httpHeaders() });
  }

  async updateEnrollment(enrollment){
    var id = enrollment.id;
    return await api.patch(`enrollments/${id}`, enrollment, { headers: ApiService.httpHeaders() });
  }

  async deleteEnrollment(id){
    return await api.delete(`enrollments/${id}`, { headers: ApiService.httpHeaders() });
  }

}

export default new EnrollmentService();
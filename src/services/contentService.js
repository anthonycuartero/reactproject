import axios from "axios";
import { API_URL } from "config.js/settings";
import ApiService from "./apiService";
import AuthService from "./authService";
const api = axios.create({
  baseURL: API_URL
})

class ContentService {
  constructor() {
    api.interceptors.response.use(
      res => {
          return res;
      },
      err => {
        if(err.response.status == 401){
          AuthService.logoutUser();
        }
      }
    );
  }

  async getContentList(moduleId, keyword) {
    let params = new URLSearchParams();
    params.append('Module', moduleId);
    params.append('Keyword', keyword);

    return await api.get('contents', { headers: ApiService.httpHeaders(), params: params });
  }

  async getContentDetails(id) {
    return await api.get(`contents/${id}`, { headers: ApiService.httpHeaders()});
  }

  async insertContent(contents) {
    return await api.post(`contents`, contents, { headers: ApiService.httpHeaders()});
  }

  async updateContent(contents) {
    var id = contents.id;
    return await api.patch(`contents/${id}`, contents, { headers: ApiService.httpHeaders()});
  }

  async deleteContent(id) {
    return await api.delete(`contents/${id}`, { headers: ApiService.httpHeaders()});
  }

}

export default new ContentService();
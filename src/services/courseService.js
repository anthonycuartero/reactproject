import axios from "axios";
import { API_URL } from "config.js/settings";
import ApiService from "./apiService";
import AuthService from "./authService";
const api = axios.create({
  baseURL: API_URL
})

class CourseService {
  constructor() {
    api.interceptors.response.use(
      res => {
          return res;
      },
      err => {
        if(err.response.status == 401){
          AuthService.logoutUser();
        }
      }
    );
  }

  async getCourseList(subjectId, keyword) {
    let params = new URLSearchParams();
    params.append('SubjectId', subjectId);
    params.append('Keyword', keyword);

    return await api.get('courses', { headers: ApiService.httpHeaders(), params: params });
  }

  async getCourseDetails(id) {
    return await api.get(`courses/${id}`, { headers: ApiService.httpHeaders()});
  }

  async insertCourse(course) {
    return await api.post(`courses`, course, { headers: ApiService.httpHeaders()});
  }

  async updateCourse(course) {
    var id = course.id;
    return await api.patch(`courses/${id}`, course, { headers: ApiService.httpHeaders()});
  }

  async deleteCourse(id) {
    return await api.delete(`courses/${id}`, { headers: ApiService.httpHeaders()});
  }

}

export default new CourseService();
import axios from "axios";
import { API_URL } from "config.js/settings";
const api = axios.create({
    baseURL: API_URL
})

class AuthService {
    constructor(){

    }

    async register(data){
        let res = await api.post('signup', data, {
            headers: {
              'Content-Type': 'application/json',
              "Access-Control-Allow-Origin": "*"
            }
        });

        return res;
    }

    async checkToken(){
        const token = localStorage.getItem('token');
        if(!token){
            window.location.href = "/auth/login";
        }
    }

    async logoutUser(){
        localStorage.removeItem('token');
        window.location.href = "/auth/login";
        
    }

}

export default new AuthService();
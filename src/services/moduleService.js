import axios from "axios";
import { API_URL } from "config.js/settings";
import ApiService from "./apiService";
import AuthService from "./authService";
const api = axios.create({
  baseURL: API_URL
})

class ModuleService {
  constructor() {
    api.interceptors.response.use(
      res => {
          return res;
      },
      err => {
        if(err.response.status == 401){
          AuthService.logoutUser();
        }
      }
    );
  }

  async getModuleList(courseId, keyword) {
    let params = new URLSearchParams();
    params.append('Course', courseId);
    params.append('Keyword', keyword);

    return await api.get('modules', { headers: ApiService.httpHeaders(), params: params });
  }

  async getModuleDetails(id) {
    return await api.get(`modules/${id}`, { headers: ApiService.httpHeaders()});
  }

  async insertModule(modules) {
    return await api.post(`modules`, modules, { headers: ApiService.httpHeaders()});
  }

  async updateModule(modules) {
    var id = modules.id;
    return await api.patch(`modules/${id}`, modules, { headers: ApiService.httpHeaders()});
  }

  async deleteModule(id) {
    return await api.delete(`modules/${id}`, { headers: ApiService.httpHeaders()});
  }

}

export default new ModuleService();
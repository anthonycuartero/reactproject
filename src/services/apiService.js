import axios from 'axios';
import { API_URL } from 'config.js/settings';
import AuthService from './authService';

const api = axios.create({
    baseURL: API_URL
})

const accessToken = localStorage.getItem('token');

class ApiService{
    constructor(){
        
    }

    httpHeaders(){
        return {
            'Content-Type': 'application/json',
            "Access-Control-Allow-Origin": "*",
            'Authorization': 'Bearer ' + accessToken
        }
    }

    
}

export default new ApiService();
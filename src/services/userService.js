import axios from "axios";
import { API_URL } from "config.js/settings";
import ApiService from "./apiService";
import AuthService from "./authService";
const api = axios.create({
  baseURL: API_URL
})

class UserService {
  constructor() {
    api.interceptors.response.use(
      res => {
          return res;
      },
      err => {
        if(err.response.status == 401){
        //   AuthService.logoutUser();
        }
      }
    );
  }

  async getUsersList(){
    return await api.get('users', { headers: ApiService.httpHeaders() } );
  }

  async getUserDetails(id){
    return await api.get(`users/${id}`, { headers: ApiService.httpHeaders() } );
  }

  async getUserDetailsByToken(){
    return await api.get(`user`, { headers: {
        'Content-Type': 'application/x-www-form-urlencoded;',
        "Access-Control-Allow-Origin": "*",
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
      } } );
  }

}

export default new UserService();
/*!

=========================================================
* Argon Dashboard React - v1.2.1
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import { Component, } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Container,
  Row,
  Col,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Input,
} from "reactstrap";


import Header from "components/Headers/Header.js";
import AuthService from "services/authService";
import SubjectService from "services/subjectService";
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import { Link } from "react-router-dom";

export class Index extends Component {

  constructor() {
    super();
    AuthService.checkToken();
    this.state = {
      subjectList: [],
      showDisplay: true,
      onUpdate:false,
      subject: {
        id: NIL_UUID,
        title: "",
        courses: [],
        isPublished: 0,
        owner: "",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      timeout:0,
      searchKeyword:'',
    };

    this.onShowDisplay = this.onShowDisplay.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
  }

  componentDidMount() {
    this.getSubjects();
  }

  handleTitleChange = (event) => {
    this.setState(prevState => ({
      subject: {
        ...prevState.subject, [event.target.name]: event.target.value
      }
    }));
  }

  async getSubjects() {
    var subjects = await SubjectService.getSubjectList(this.state.searchKeyword);
    console.log(subjects);
    this.setState({ subjectList: subjects.data.data });
  }

  onShowDisplay() {
    if (this.state.showDisplay) {
      this.setState({ showDisplay: false });
    } else {
      this.setState({ subject : {} })
      this.setState({ showDisplay: true });
      this.setState({ onUpdate: false });
    }

  }

  async insertSubject() {
    var response = await SubjectService.insertSubject(this.state.subject);
    if(response){
      alert("Successfully saved");
      this.getSubjects();
      this.onShowDisplay();
    }
  }

  async deleteSubject(subject){
    var response = await SubjectService.deleteSubject(subject.id);
    if(response){
      alert("Successfully deleted");
      this.getSubjects();
    }
  }

  async onUpdateSubject(subject){
    this.setState({ subject: subject });
    this.setState({ onUpdate: true });
    this.setState({ showDisplay: false });
  }

  async updateSubject(){
    var response = await SubjectService.updateSubject(this.state.subject);
    if(response){
      alert("Successfully updated");
      this.getSubjects();
      this.onShowDisplay();
    }
  }

  doSearch = (evt) => {
    this.setState({ searchKeyword: evt.target.value });
    if(this.state.timeout) clearTimeout(this.state.timeout);
    this.state.timeout = setTimeout(() => {
      this.getSubjects();
    }, 800);
  }


  render() {
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          <Row>
            <Col xl="12">
              <Card className="shadow">
                <CardHeader className="bg-transparent">
                  <Row className="align-items-center">
                    <div className="col-lg-2">
                      <h2 className="mb-0">My Subjects</h2>
                    </div>
                    <form  class="navbar-search col-lg-5">
                      <div class="form-group mb-0" >
                        <div class="input-group input-group-alternative">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                          </div>
                          <input class="form-control" onChange={evt => this.doSearch(evt)} placeholder="Search" type="text" />
                        </div>
                      </div>
                    </form>
                    <div className="col-lg-1"></div>
                    <div className="col-lg-4">
                      {this.state.showDisplay && <a className="text-white btn btn-md bg-gradient-success" onClick={this.onShowDisplay.bind(this)} ><i className="ni ni-fat-add"></i> Add Subject</a>}
                      {this.state.showDisplay == false && <a className="text-white btn btn-md bg-gradient-danger" onClick={this.onShowDisplay.bind(this)} ><i className="ni ni-bold-left"></i> Cancel</a>}
                    </div>
                  </Row>
                </CardHeader>
                <CardBody>
                  {this.state.showDisplay &&
                    <div className="row">
                      {this.state.subjectList.map((subject) => (
                        <div className="col-sm-4" style={{ "marginTop": "25px", "marginBottom": "25px" }} >
                          <div className="card shadow">
                            <div className="card-header bg-transparent">
                              <div className="row align-items-center">
                                <UncontrolledDropdown className="float-right">
                                  <DropdownToggle
                                    className="btn-icon-only"
                                    href="#pablo"
                                    role="button"
                                    size="sm"
                                    color=""
                                    onClick={(e) => e.preventDefault()}
                                  >
                                    <i className="fas fa-ellipsis-v" />
                                  </DropdownToggle>
                                  <DropdownMenu className="dropdown-menu-arrow" right>
                                    <DropdownItem
                                      href="#pablo"
                                      onClick={this.onUpdateSubject.bind(this, subject)}
                                    >
                                      Edit
                                    </DropdownItem>
                                    <DropdownItem
                                      href="#pablo"
                                      onClick={this.deleteSubject.bind(this, subject)}
                                    >
                                      Delete
                                    </DropdownItem>
                                  </DropdownMenu>
                                </UncontrolledDropdown>
                                <div className="col-lg-12 text-center">
                                  <a href="#">
                                    <img src={require('../assets/img/brand/subject_logo.png').default} className="img-thumbnail" style={{ "width": "200px", "height": "200px" }} />
                                    <h3>{subject.title}</h3>
                                  </a>
                                </div>
                                <div className="col-lg-12">
                                  <h4>Description: {subject.description}<span className="text-success"></span></h4>
                                  <h4>Created: {subject.createdAt}</h4>
                                  <h4>Updated: {subject.updatedAt}</h4>
                                  <div className="text-center">
                                    <Link to={`/admin/course/${subject.id}`} className="text-white text-center btn btn-md bg-gradient-primary">View Courses</Link>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                      }
                    </div>
                  }

                  {this.state.showDisplay == false &&
                    <div className="row">
                      <div className="col-lg-3"></div>
                      <div className="col-lg-6">
                        <label
                          className="form-control-label text-center"
                          htmlFor="input-username"
                        >
                          Subject Title
                        </label>
                        <Input
                          className="form-control-alternative"
                          value={this.state.subject.title}
                          name="title"
                          placeholder="Input title here ..."
                          type="text"
                          onChange={this.handleTitleChange}
                        />
                        {!this.state.onUpdate && <a className="text-white btn btn-md bg-gradient-success" style={{ "marginTop": "10px" }} onClick={this.insertSubject.bind(this)} ><i className="ni ni-fat-save"></i> Add Subject</a>}
                        {this.state.onUpdate && <a className="text-white btn btn-md bg-gradient-success" style={{ "marginTop": "10px" }} onClick={this.updateSubject.bind(this)} ><i className="ni ni-fat-save"></i> Update Subject</a>}
                      </div>
                      <div className="col-lg-1"></div>
                    </div>
                  }
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  };

}

export default Index;

import { Component, } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Container,
  Row,
  Col,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Input,
} from "reactstrap";




import Header from "components/Headers/Header.js";
import AuthService from "services/authService";
import ModuleService from "services/moduleService";
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import { Link, useParams } from "react-router-dom";

export class Module extends Component {

  constructor(props) {
    super(props);
    AuthService.checkToken();

    this.state = {
      moduleList: [],
      showDisplay: true,
      onUpdate:false,
      module: {
        id:NIL_UUID, 
        title:'',
        duration:0,
        courseId:this.props.match.params.courseId,
        subjectId:this.props.match.params.subjectId,
        contents:'',
        isPublished:0,
        courseTitle:'',
        createdAt:new Date(),
        updatedAt:new Date(),
        version:0,
      },
      timeout:0,
      searchKeyword:'',
    };

    this.onShowDisplay = this.onShowDisplay.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
  }

  componentDidMount() {
    this.getModules();
  }

  handleTitleChange = (event) => {
    this.setState(prevState => ({module: {...prevState.module, [event.target.name]: event.target.value}}));
    this.setState(prevState => ({module: {...prevState.module, courseId: this.props.match.params.courseId}}));
  }

  handleNumberChange = (event) => {
    this.setState(prevState => ({module: {...prevState.module, [event.target.name]: +event.target.value}}));
    this.setState(prevState => ({module: {...prevState.module, courseId: this.props.match.params.courseId}}));
  }



  async getModules() {
    var modules = await ModuleService.getModuleList(this.props.match.params.courseId,this.state.searchKeyword);
    this.setState({ moduleList: modules.data.data });
  }

  onShowDisplay() {
    if (this.state.showDisplay) {
      this.setState({ showDisplay: false });
    } else {
      this.setState({ module : {} })
      this.setState({ showDisplay: true });
      this.setState({ onUpdate: false });
    }

  }

  async insertModule() {

    var response = await ModuleService.insertModule(this.state.module);
    if(response){
      alert("Successfully saved");
      this.getModules();
      this.onShowDisplay();
    }
  }

  async deleteModule(module){
    var response = await ModuleService.deleteModule(module.id);
    if(response){
      alert("Successfully deleted");
      this.getModules();
    }
  }

  async onUpdateModule(module){
    this.setState({ module: module });
    this.setState({ onUpdate: true });
    this.setState({ showDisplay: false });
  }

  async updateModule(){
    var response = await ModuleService.updateModule(this.state.module);
    if(response){
      alert("Successfully updated");
      this.getModules();
      this.onShowDisplay();
    }
  }

  doSearch = (evt) => {
    this.setState({ searchKeyword: evt.target.value });
    if(this.state.timeout) clearTimeout(this.state.timeout);
    this.state.timeout = setTimeout(() => {
      this.getModules();
    }, 800);
  }


  render() {
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          <Row>
            <Col xl="12">
              <Card className="shadow">
                <CardHeader className="bg-transparent">
                  <Row className="align-items-center">
                    <div className="col-lg-2">
                      <h2 className="mb-0"> <Link to={`/admin/course/${this.state.module.subjectId}`}> <i className="ni ni-bold-left"></i> </Link> My Modules</h2>
                    </div>
                    <form  class="navbar-search col-lg-5">
                      <div class="form-group mb-0" >
                        <div class="input-group input-group-alternative">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                          </div>
                          <input class="form-control" onChange={evt => this.doSearch(evt)} placeholder="Search" type="text" />
                        </div>
                      </div>
                    </form>
                    <div className="col-lg-1"></div>
                    <div className="col-lg-4">
                      {this.state.showDisplay && <a className="text-white btn btn-md bg-gradient-success" onClick={this.onShowDisplay.bind(this)} ><i className="ni ni-fat-add"></i> Add Module</a>}
                      {this.state.showDisplay == false && <a className="text-white btn btn-md bg-gradient-danger" onClick={this.onShowDisplay.bind(this)} ><i className="ni ni-bold-left"></i> Cancel</a>}
                    </div>
                  </Row>
                </CardHeader>
                <CardBody>
                  {this.state.showDisplay &&
                    <div className="row">
                      {this.state.moduleList.map((module) => (
                        <div className="col-sm-4" style={{ "marginTop": "25px", "marginBottom": "25px" }} >
                          <div className="card shadow">
                            <div className="card-header bg-transparent">
                              <div className="row align-items-center">
                                <UncontrolledDropdown className="float-right">
                                  <DropdownToggle
                                    className="btn-icon-only"
                                    href="#pablo"
                                    role="button"
                                    size="sm"
                                    color=""
                                    onClick={(e) => e.preventDefault()}
                                  >
                                    <i className="fas fa-ellipsis-v" />
                                  </DropdownToggle>
                                  <DropdownMenu className="dropdown-menu-arrow" right>
                                    <DropdownItem
                                      href="#pablo"
                                      onClick={this.onUpdateModule.bind(this, module)}
                                    >
                                      Edit
                                    </DropdownItem>
                                    <DropdownItem
                                      href="#pablo"
                                      onClick={this.deleteModule.bind(this, module)}
                                    >
                                      Delete
                                    </DropdownItem>
                                  </DropdownMenu>
                                </UncontrolledDropdown>
                                <div className="col-lg-12 text-center">
                                  <a href="#">
                                    <img src={require('../../assets/img/brand/module_logo.png').default} className="img-thumbnail" style={{ "width": "200px", "height": "200px" }} />
                                    <h3>{module.title}</h3>
                                  </a>
                                </div>
                                <div className="col-lg-12">
                                  {/* <h4>Title: {module.title}<span className="text-success"></span></h4> */}
                                  <h4>Created: {module.createdAt}</h4>
                                  <h4>Updated: {module.updatedAt}</h4>
                                  <div className="text-center">
                                  <Link to={`/admin/content/${module.id}/${module.courseId}/${this.props.match.params.subjectId}`} className="text-white text-center btn btn-md bg-gradient-primary">View Contents</Link>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                      }
                    </div>
                  }

                  {this.state.showDisplay == false &&
                    <div className="row">
                      <div className="col-lg-3"></div>
                      <div className="col-lg-6">
                        <label
                          className="form-control-label text-center"
                          htmlFor="input-username"
                        >
                          Module Title
                        </label>
                        <Input
                          className="form-control-alternative"
                          value={this.state.module.title}
                          name="title"
                          placeholder="Input title here ..."
                          type="text"
                          onChange={this.handleTitleChange}
                        />
                        <label
                          className="form-control-label text-center"
                          htmlFor="input-username"
                        >
                          Duration
                        </label>
                        <Input style={{ "marginTop": "10px" }}
                          className="form-control-alternative"
                          value={this.state.module.duration}
                          name="duration"
                          placeholder="0"
                          type="number"
                          onChange={this.handleNumberChange}
                        />

                        {!this.state.onUpdate && <a className="text-white btn btn-md bg-gradient-success" style={{ "marginTop": "10px" }} onClick={this.insertModule.bind(this)} ><i className="ni ni-fat-save"></i> Add Module</a>}
                        {this.state.onUpdate && <a className="text-white btn btn-md bg-gradient-success" style={{ "marginTop": "10px" }} onClick={this.updateModule.bind(this)} ><i className="ni ni-fat-save"></i> Update Module</a>}
                      </div>
                      <div className="col-lg-1"></div>
                    </div>
                  }
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  };

}

export default Module;
import { Component, } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Container,
  Row,
  Col,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Input,
} from "reactstrap";




import Header from "components/Headers/Header.js";
import AuthService from "services/authService";
import CourseService from "services/courseService";
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import { Link, useParams } from "react-router-dom";
import EnrollmentService from "services/enrollmentService";

export class Course extends Component {

  constructor(props) {
    super(props);
    AuthService.checkToken();

    this.state = {
      courseList: [],
      showDisplay: true,
      onUpdate: false,
      course: {
        id: NIL_UUID,
        subjectId: this.props.match.params.subjectId,
        title: '',
        description: '',
        icon: '',
        author: '',
        duration: '',
        isPublished: 0,
        modules: [],
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      timeout: 0,
      searchKeyword: '',
      isStudent: false,
    };

    this.onShowDisplay = this.onShowDisplay.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
  }

  componentDidMount() {
    this.getCourses();
    var role = localStorage.getItem('role');
    if (role == 'Student') {
      this.setState({ isStudent: true });
    }
  }

  handleTitleChange = (event) => {
    this.setState(prevState => ({
      course: {
        ...prevState.course, [event.target.name]: event.target.value
      }
    }));
    this.setState(prevState => ({ course: { ...prevState.course, subjectId: this.props.match.params.subjectId } }));
  }

  async getCourses() {
    var courses = await CourseService.getCourseList(this.props.match.params.subjectId, this.state.searchKeyword);
    this.setState({ courseList: courses.data.data });
    console.log(this.state.courseList);
  }

  onShowDisplay() {
    if (this.state.showDisplay) {
      this.setState({ showDisplay: false });
    } else {
      this.setState({ course: {} })
      this.setState({ showDisplay: true });
      this.setState({ onUpdate: false });
    }

  }

  async insertCourse() {
    var response = await CourseService.insertCourse(this.state.course);
    if (response) {
      alert("Successfully saved");
      this.getCourses();
      this.onShowDisplay();
    }
  }

  async deleteCourse(course) {
    var response = await CourseService.deleteCourse(course.id);
    if (response) {
      alert("Successfully deleted");
      this.getCourses();
    }
  }

  async onUpdateCourse(course) {
    this.setState({ course: course });
    this.setState({ onUpdate: true });
    this.setState({ showDisplay: false });
  }

  async updateCourse() {
    var response = await CourseService.updateCourse(this.state.course);
    if (response) {
      alert("Successfully updated");
      this.getCourses();
      this.onShowDisplay();
    }
  }

  async enrollCourse(courseId){
    var response = await EnrollmentService.insertEnrollment({courseId:courseId});
    if(response){
      alert("Successfully enrolled");
      this.getCourses();
    }else{
      alert("You already enrolled on this course.")
    }
  }

  doSearch = (evt) => {
    this.setState({ searchKeyword: evt.target.value });
    if (this.state.timeout) clearTimeout(this.state.timeout);
    this.state.timeout = setTimeout(() => {
      this.getCourses();
    }, 800);
  }


  render() {
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          <Row>
            <Col xl="12">
              <Card className="shadow">
                <CardHeader className="bg-transparent">
                  <Row className="align-items-center">
                    <div className="col-lg-2">
                      <h2 className="mb-0"> {this.state.isStudent == false && <Link to={`/admin/subject`}> <i className="ni ni-bold-left"></i> </Link> } My Courses</h2>
                    </div>
                    {this.state.isStudent == false && <form class="navbar-search col-lg-5">
                      <div class="form-group mb-0" >
                        <div class="input-group input-group-alternative">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                          </div>
                          <input class="form-control" onChange={evt => this.doSearch(evt)} placeholder="Search" type="text" />
                        </div>
                      </div>
                    </form>}
                    
                    <div className="col-lg-1"></div>
                    {this.state.isStudent == false &&
                      <div className="col-lg-4">
                        {this.state.showDisplay && <a className="text-white btn btn-md bg-gradient-success" onClick={this.onShowDisplay.bind(this)} ><i className="ni ni-fat-add"></i> Add Course</a>}
                        {this.state.showDisplay == false && <a className="text-white btn btn-md bg-gradient-danger" onClick={this.onShowDisplay.bind(this)} ><i className="ni ni-bold-left"></i> Cancel</a>}
                      </div>
                    }

                  </Row>
                </CardHeader>
                <CardBody>
                  {this.state.showDisplay &&
                    <div className="row">
                      {this.state.courseList.map((course) => (
                        <div className="col-sm-4" style={{ "marginTop": "25px", "marginBottom": "25px" }} >
                          <div className="card shadow">
                            <div className="card-header bg-transparent">
                              <div className="row align-items-center">
                              {this.state.isStudent == false &&
                                <UncontrolledDropdown className="float-right">
                                  <DropdownToggle
                                    className="btn-icon-only"
                                    href="#pablo"
                                    role="button"
                                    size="sm"
                                    color=""
                                    onClick={(e) => e.preventDefault()}
                                  >
                                    <i className="fas fa-ellipsis-v" />
                                  </DropdownToggle>
                                  <DropdownMenu className="dropdown-menu-arrow" right>
                                    <DropdownItem
                                      href="#pablo"
                                      onClick={this.onUpdateCourse.bind(this, course)}
                                    >
                                      Edit
                                    </DropdownItem>
                                    <DropdownItem
                                      href="#pablo"
                                      onClick={this.deleteCourse.bind(this, course)}
                                    >
                                      Delete
                                    </DropdownItem>
                                  </DropdownMenu>
                                </UncontrolledDropdown>
  }
                                
                                <div className="col-lg-12 text-center">
                                  <a href="#">
                                    <img src={require('../../assets/img/brand/course_logo.jpg').default} className="img-thumbnail" style={{ "width": "200px", "height": "200px" }} />
                                    <h3>{course.title}</h3>
                                  </a>
                                </div>
                                <div className="col-lg-12">
                                  <h4>Subj Owner: {course.author}<span className="text-success"></span></h4>
                                  <h4>Created: {course.createdAt}</h4>
                                  <h4>Updated: {course.updatedAt}</h4>
                                  
                                  <div className="text-center">
                                  {this.state.isStudent == false && <Link to={`/admin/module/${course.id}/${course.subjectId}`} className="text-white text-center btn btn-md bg-gradient-primary">View Modules</Link>}
                                  {this.state.isStudent && <a onClick={this.enrollCourse.bind(this, course.id)} class="text-white btn btn-md bg-gradient-success">Enroll Course</a> }
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                      }
                    </div>
                  }

                  {this.state.showDisplay == false &&
                    <div className="row">
                      <div className="col-lg-3"></div>
                      <div className="col-lg-6">
                        <label
                          className="form-control-label text-center"
                          htmlFor="input-username"
                        >
                          Course Title
                        </label>
                        <Input
                          className="form-control-alternative"
                          value={this.state.course.title}
                          name="title"
                          placeholder="Input title here ..."
                          type="text"
                          onChange={this.handleTitleChange}
                        />
                        {!this.state.onUpdate && <a className="text-white btn btn-md bg-gradient-success" style={{ "marginTop": "10px" }} onClick={this.insertCourse.bind(this)} ><i className="ni ni-fat-save"></i> Add Course</a>}
                        {this.state.onUpdate && <a className="text-white btn btn-md bg-gradient-success" style={{ "marginTop": "10px" }} onClick={this.updateCourse.bind(this)} ><i className="ni ni-fat-save"></i> Update Course</a>}
                      </div>
                      <div className="col-lg-1"></div>
                    </div>
                  }
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  };

}

export default Course;
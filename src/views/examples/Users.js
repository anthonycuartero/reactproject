/*!

=========================================================
* Argon Dashboard React - v1.2.1
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// reactstrap components
import {
  Card,
  CardHeader,
  Media,
  Table,
  Container,
  Row,
  UncontrolledTooltip,
  Button
} from "reactstrap";
// core components
import Header from "components/Headers/Header.js";
import { Component } from "react";
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import UserService from "services/userService";

export class Users extends Component {

  constructor() {
    super();
    this.state = {
      userList: [],
      user: {
        id: NIL_UUID,
        email:'',
        role:'',
        firstName:'',
        lastName:'',
      }
    };
  }

  componentDidMount() {
    this.getUsersList();
  }


  async getUsersList(){
    var users = await UserService.getUsersList();
    this.setState({ userList: users.data.data });
  }

  async getUserDetails(id){
    var users = await UserService.getUserDetails(id);
    if(users){
      this.setState({ user: users.data });
    }
  }

  render() {
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          {/* Table */}
          <Row>
            <div className="col-lg-8">
              <Card className="shadow">
                <CardHeader className="border-0">
                  <h3 className="mb-0">User Lists</h3>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col" >Email</th>
                      <th scope="col" >Firstname</th>
                      <th scope="col" >Lastname</th>
                      <th scope="col" className="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  {this.state.userList.map((user) => (
                    <tr>
                      <th scope="row">
                        {user.email}
                      </th>
                      <td > {user.firstName} </td>
                      <td > {user.lastName} </td>
                      <td>
                        <div className="text-center">
                          <Button className="mt-4" color="primary" type="button" onClick={this.getUserDetails.bind(this, user.id)}>
                            VIEW DETAILS
                          </Button>
                        </div>
                      </td>
                    </tr>
                  ))}
                  </tbody>
                </Table>
              </Card>
            </div>
            <div className="col-xl-4 order-xl-2 mb-5 mb-xl-0">
              <div className="card card-profile shadow">
                <div className="row justify-content-center">
                  <div className="col-lg-3 order-lg-2">
                    <div className="card-profile-image">
                      <a href="javascript:void(0)">
                        <img src={require('assets/img/theme/team-4-800x800.jpg').default} className="rounded-circle"/>
                      </a>
                    </div>
                    </div>
                  </div>
                  <div className="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
                    <div className="d-flex justify-content-between">
                      <a href="javascript:void(0)" className="btn btn-sm btn-info mr-4">Connect</a>
                      <a href="javascript:void(0)" className="btn btn-sm btn-default float-right">Message</a>
                    </div>
                  </div>
                  <div className="card-body pt-0 pt-md-4">
                    <div className="row">
                      <div className="col">
                        <div className="card-profile-stats d-flex justify-content-center mt-md-5">
                          <div>
                            <span className="heading">22</span>
                            <span className="description">Friends</span>
                          </div>
                          <div>
                            <span className="heading">10</span>
                            <span className="description">Photos</span>
                          </div>
                          <div>
                            <span className="heading">89</span>
                            <span className="description">Comments</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="text-center">
                      <h3>
                        { this.state.user.firstName } { this.state.user.lastName }
                      </h3>
                      <div className="h5 font-weight-300">
                        <i className="ni location_pin mr-2"></i>
                        { this.state.user.role }
                      </div>
                      <div className="h5 mt-4">
                        <i className="ni business_briefcase-24 mr-2"></i>
                        { this.state.user.email }
                      </div>
                      <div>
                        <i className="ni education_hat mr-2"></i>Arcanys University
                      </div>
                      <hr className="my-4" />
                    </div>
                  </div>
                </div>
              </div>


          </Row>
        </Container>
      </>
        );
  };
}



        export default Users;

import { Component, } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Container,
  Row,
  Col,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Input,
} from "reactstrap";




import Header from "components/Headers/Header.js";
import AuthService from "services/authService";
import ModuleService from "services/moduleService";
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import { Link, useParams } from "react-router-dom";
import contentService from "services/contentService";

export class StudentModule extends Component {

  constructor(props) {
    super(props);
    AuthService.checkToken();

    this.state = {
      moduleList: [],
      showDisplay: true,
      onUpdate:false,
      module: [],
      contentList: [],
      timeout:0,
      searchKeyword:'',
    };

  }

  componentDidMount() {
    this.getModuleList();
  }

  async getModuleList(){
    var modules = await ModuleService.getModuleList(this.props.match.params.courseId,this.state.searchKeyword);
    this.setState({ module: modules.data.data });
  }

  async getContentList(moduleId){
    var contentList = await contentService.getContentList(moduleId,"");
    this.setState({ contentList: contentList.data.data });
  }



  render() {
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
        <div className="row">
    <div className="col-xl-3">
      <div className="card shadow">
        <div className="card-body">
          
          <div >
            <h1 className="text-center">No modules available</h1>
          </div>
          <div className="row" >
          {this.state.module.map((module) => (
            <div className="col-sm-12" style={{"margin-top":"25px","margin-bottom":"25px"}}>
              <div className="card shadow">
                <div className="card-header bg-transparent">
                  <div className="row align-items-center">
                      <div className="col-lg-12">
                        <h4 className="text-center">{module.title}</h4>
                        <div className="text-center">
                          <a onClick={this.getContentList.bind(this, module.id)} className="text-white btn btn-sm bg-gradient-success">View</a>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          ))}
          </div>
        </div>
      </div>
    </div>
    <div className="col-xl-9">
      <div className="card shadow">
        <div className="card-body">
          <div >
            <h1 className="text-center">No content available</h1>
          </div>
          <div className="row">
          {this.state.contentList.map((content) => (
            <div className="col-sm-12" style={{"margin-top":"25px","margin-bottom":"25px"}} >
            <div className="card shadow">
              <div className="card-header bg-transparent">
                <div className="row align-items-center">
                    <div className="col-lg-12">
                      <h4 className="text-center">{content.contentName}</h4>
                      <h4 className="text-center">{content.type}</h4>
                    </div>
                </div>
              </div>
            </div>
          </div>
          ))}

            
          </div>
          
        </div>
      </div>
    </div>
  </div>
        </Container>
      </>
    );
  };

}

export default StudentModule;
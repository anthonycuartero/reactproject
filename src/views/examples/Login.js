
import { Component } from "react";
import axios from 'axios';
import { LOGIN_URL } from "config.js/settings";
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col,
} from "reactstrap";
import UserService from "services/userService";
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';


const api = axios.create({
  baseURL: LOGIN_URL
})

class Login extends Component {

  constructor(){
    super();

    this.state = {
       username: '',
       password:'',
       grant_type:'password',
       client_id:'postman',
       client_secret:'secret',
       scope:'apiscope',
    };

    this.handleChange = this.handleChange.bind(this);
  }



  componentDidMount() {
    if(localStorage.getItem('token')){
      window.location.href = "/admin/index"
    }
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  }

  loginUser = async() =>{
    console.log(this.state);

    var data = new URLSearchParams();
    data.append('username', this.state.username)
    data.append('password', this.state.password)
    data.append('grant_type', this.state.grant_type)
    data.append('client_id', this.state.client_id)
    data.append('client_secret', this.state.client_secret)
    data.append('scope', this.state.scope);

    let res = await api.post('', data, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;',
        "Access-Control-Allow-Origin": "*"
      }
    });

    if(res){
      localStorage.setItem('token', res.data.access_token);
      this.getUserDetails();
      
    }
  }

  async getUserDetails() {
    var user = await UserService.getUserDetailsByToken();
    if(user){
      console.log(user);
      localStorage.setItem('role', user.data.role);
      alert("Successfully Login");
      if(user.data.role == 'Student'){
        window.location.href = '/admin/course/' + NIL_UUID;
      }else{
        window.location.href = "/admin/index";
      }
      
    }
  }


  render() {
    return (
      <>
        <Col lg="5" md="7">
          <Card className="bg-secondary shadow border-0">
            <CardBody className="px-lg-5 py-lg-5">
              <div className="text-center text-muted mb-4">
                <small>Sign in with credentials</small>
              </div>
              <Form role="form">
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Email"
                      type="text"
                      name="username"
                      value={this.state.username}
                      autoComplete="new-email"
                      onChange={this.handleChange}
                    />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Password"
                      type="password"
                      value={this.state.password}
                      name="password"
                      autoComplete="new-password"
                      onChange={this.handleChange}
                    />
                  </InputGroup>
                </FormGroup>
                <div className="custom-control custom-control-alternative custom-checkbox">
                  <input
                    className="custom-control-input"
                    id=" customCheckLogin"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor=" customCheckLogin"
                  >
                    <span className="text-muted">Remember me</span>
                  </label>
                </div>
                <div className="text-center">
                  <Button className="my-4" color="primary" type="button"
                    onClick={this.loginUser.bind(this)}>
                    Sign in
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
          <Row className="mt-3">
            <Col xs="6">
              <a
                className="text-light"
                href="#pablo"
                onClick={(e) => e.preventDefault()}
              >
                <small>Forgot password?</small>
              </a>
            </Col>
            <Col className="text-right" xs="6">
              <a
                className="text-light"
                href="/auth/register"
              >
                <small>Create new account</small>
              </a>
            </Col>
          </Row>
        </Col>
      </>
    )
  };

}



export default Login;

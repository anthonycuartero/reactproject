import { Component, } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Container,
  Row,
  Col,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Input,
} from "reactstrap";




import Header from "components/Headers/Header.js";
import AuthService from "services/authService";
import ContentService from "services/contentService";
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import { Link, useParams } from "react-router-dom";

export class Content extends Component {

  constructor(props) {
    super(props);
    AuthService.checkToken();

    this.state = {
      contentList: [],
      showDisplay: true,
      onUpdate:false,
      content: {
        id:NIL_UUID,
        contentName:'',
        content:'',
        type:'',
        isPublished:0,
        createdAt:new Date(),
        updatedAt:new Date(),
        moduleId:this.props.match.params.moduleId,
        courseId:this.props.match.params.courseId,
        subjectId:this.props.match.params.subjectId,
      },
      timeout:0,
      searchKeyword:'',
    };

    this.onShowDisplay = this.onShowDisplay.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
  }

  componentDidMount() {
    this.getContents();
  }

  handleTitleChange = (event) => {
    this.setState(prevState => ({content: {...prevState.content, [event.target.name]: event.target.value}}));
    this.setState(prevState => ({content: {...prevState.content, moduleId: this.props.match.params.moduleId}}));
    this.setState(prevState => ({content: {...prevState.content, courseId: this.props.match.params.courseId}}));
    this.setState(prevState => ({content: {...prevState.content, subjectId: this.props.match.params.subjectId}}));
  }


  async getContents() {
    var content = await ContentService.getContentList(this.props.match.params.moduleId,this.state.searchKeyword);
    this.setState({ contentList: content.data.data });
  }

  onShowDisplay() {
    if (this.state.showDisplay) {
      this.setState({ showDisplay: false });
    } else {
      this.setState({ content : {} })
      this.setState({ showDisplay: true });
      this.setState({ onUpdate: false });
    }

  }

  async insertContent() {

    var response = await ContentService.insertContent(this.state.content);
    if(response){
      alert("Successfully saved");
      this.getContents();
      this.onShowDisplay();
    }
  }

  async deleteContent(content){
    var response = await ContentService.deleteContent(content.id);
    if(response){
      alert("Successfully deleted");
      this.getContents();
    }
  }

  async onUpdateContent(content){
    this.setState({ content: content });
    this.setState({ onUpdate: true });
    this.setState({ showDisplay: false });
  }

  async updateContent(){
    var response = await ContentService.updateContent(this.state.content);
    if(response){
      alert("Successfully updated");
      this.getContents();
      this.onShowDisplay();
    }
  }

  doSearch = (evt) => {
    this.setState({ searchKeyword: evt.target.value });
    if(this.state.timeout) clearTimeout(this.state.timeout);
    this.state.timeout = setTimeout(() => {
      this.getContents();
    }, 800);
  }


  render() {
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          <Row>
            <Col xl="12">
              <Card className="shadow">
                <CardHeader className="bg-transparent">
                  <Row className="align-items-center">
                    <div className="col-lg-2">
                      <h2 className="mb-0"> <Link to={`/admin/module/${this.state.content.courseId}/${this.state.content.subjectId}`}> <i className="ni ni-bold-left"></i> </Link> My Contents</h2>
                    </div>
                    <form  class="navbar-search col-lg-5">
                      <div class="form-group mb-0" >
                        <div class="input-group input-group-alternative">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-search"></i></span>
                          </div>
                          <input class="form-control" onChange={evt => this.doSearch(evt)} placeholder="Search" type="text" />
                        </div>
                      </div>
                    </form>
                    <div className="col-lg-1"></div>
                    <div className="col-lg-4">
                      {this.state.showDisplay && <a className="text-white btn btn-md bg-gradient-success" onClick={this.onShowDisplay.bind(this)} ><i className="ni ni-fat-add"></i> Add Content</a>}
                      {this.state.showDisplay == false && <a className="text-white btn btn-md bg-gradient-danger" onClick={this.onShowDisplay.bind(this)} ><i className="ni ni-bold-left"></i> Cancel</a>}
                    </div>
                  </Row>
                </CardHeader>
                <CardBody>
                  {this.state.showDisplay &&
                    <div className="row">
                      {this.state.contentList.map((content) => (
                        <div className="col-sm-4" style={{ "marginTop": "25px", "marginBottom": "25px" }} >
                          <div className="card shadow">
                            <div className="card-header bg-transparent">
                              <div className="row align-items-center">
                                <UncontrolledDropdown className="float-right">
                                  <DropdownToggle
                                    className="btn-icon-only"
                                    href="#pablo"
                                    role="button"
                                    size="sm"
                                    color=""
                                    onClick={(e) => e.preventDefault()}
                                  >
                                    <i className="fas fa-ellipsis-v" />
                                  </DropdownToggle>
                                  <DropdownMenu className="dropdown-menu-arrow" right>
                                    <DropdownItem
                                      href="#pablo"
                                      onClick={this.onUpdateContent.bind(this, content)}
                                    >
                                      Edit
                                    </DropdownItem>
                                    <DropdownItem
                                      href="#pablo"
                                      onClick={this.deleteContent.bind(this, content)}
                                    >
                                      Delete
                                    </DropdownItem>
                                  </DropdownMenu>
                                </UncontrolledDropdown>
                                <div className="col-lg-12 text-center">
                                  <a href="#">
                                    <img src={require('../../assets/img/brand/content_logo.png').default} className="img-thumbnail" style={{ "width": "200px", "height": "200px" }} />
                                    <h3>{content.contentName}</h3>
                                  </a>
                                </div>
                                <div className="col-lg-12">
                                  <h4>Type: {content.type}<span className="text-success"></span></h4>
                                  <h4>Created: {content.createdAt}</h4>
                                  <h4>Updated: {content.updatedAt}</h4>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                      }
                    </div>
                  }

                  {this.state.showDisplay == false &&
                    <div className="row">
                      <div className="col-lg-3"></div>
                      <div className="col-lg-6">
                        <label
                          className="form-control-label text-center"
                          htmlFor="input-username"
                        >
                          Content Title
                        </label>
                        <Input
                          className="form-control-alternative"
                          value={this.state.content.content}
                          name="content"
                          placeholder="Input title here ..."
                          type="text"
                          onChange={this.handleTitleChange}
                        />
                        <label style={{ "marginTop": "10px" }}
                          className="form-control-label text-center"
                          htmlFor="input-username"
                        >
                          Content Type
                        </label>
                        <Input 
                          className="form-control-alternative"
                          value={this.state.content.type}
                          name="type"
                          placeholder="Input type here ..."
                          type="text"
                          onChange={this.handleTitleChange}
                        />

                        {!this.state.onUpdate && <a className="text-white btn btn-md bg-gradient-success" style={{ "marginTop": "10px" }} onClick={this.insertContent.bind(this)} ><i className="ni ni-fat-save"></i> Add Content</a>}
                        {this.state.onUpdate && <a className="text-white btn btn-md bg-gradient-success" style={{ "marginTop": "10px" }} onClick={this.updateContent.bind(this)} ><i className="ni ni-fat-save"></i> Update Content</a>}
                      </div>
                      <div className="col-lg-1"></div>
                    </div>
                  }
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  };

}

export default Content;
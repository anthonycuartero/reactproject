import { Component, } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  Container,
  Row,
  Col,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Input,
} from "reactstrap";




import Header from "components/Headers/Header.js";
import AuthService from "services/authService";
import CourseService from "services/courseService";
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import { Link, useParams } from "react-router-dom";
import EnrollmentService from "services/enrollmentService";

export class StudentCourse extends Component {

  constructor(props) {
    super(props);
    AuthService.checkToken();

    this.state = {
      courseList: [],
      showDisplay: true,
      onUpdate: false,
      course: {
        id: NIL_UUID,
        subjectId: this.props.match.params.subjectId,
        title: '',
        description: '',
        icon: '',
        author: '',
        duration: '',
        isPublished: 0,
        modules: [],
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    };
  }

  componentDidMount() {
    this.getEnrollmentList();
  }

  async getEnrollmentList(){
    var courses = await EnrollmentService.getEnrollmentList(NIL_UUID);
    this.setState({ courseList: courses.data.data });
    console.log(this.state.courseList);
  }


  render() {
    return (
      <>
        <Header />
        {/* Page content */}
        <Container className="mt--7" fluid>
          <Row>
            <Col xl="12">
              <Card className="shadow">
                <CardHeader className="bg-transparent">
                  <Row className="align-items-center">
                    <div className="col-lg-6">
                      <h2 className="mb-0"> Enrolled Courses</h2>
                    </div>
                  </Row>
                </CardHeader>
                <CardBody>
                  {this.state.showDisplay &&
                    <div className="row">
                      {this.state.courseList.map((course) => (
                        <div className="col-sm-4" style={{ "marginTop": "25px", "marginBottom": "25px" }} >
                          <div className="card shadow">
                            <div className="card-header bg-transparent">
                              <div className="row align-items-center">
                                
                                <div className="col-lg-12 text-center">
                                  <a href="#">
                                    <img src={require('../../assets/img/brand/course_logo.jpg').default} className="img-thumbnail" style={{ "width": "200px", "height": "200px" }} />
                                    <h3>{course.title}</h3>
                                  </a>
                                </div>
                                <div className="col-lg-12">
                                  <h4>Subj Owner: {course.author}<span className="text-success"></span></h4>
                                  <h4>Created: {course.createdAt}</h4>
                                  <h4>Updated: {course.updatedAt}</h4>
                                  
                                  <div className="text-center">
                                   <Link to={`/admin/StudentModule/${course.id}`} className="text-white text-center btn btn-md bg-gradient-primary">View Modules</Link>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                      }
                    </div>
                  }

                
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      </>
    );
  };

}

export default StudentCourse;
import Index from "views/Index.js";
import Profile from "views/examples/Profile.js";
import Maps from "views/examples/Maps.js";
import Register from "views/examples/Register.js";
import Login from "views/examples/Login.js";
import Icons from "views/examples/Icons.js";
import Users from "views/examples/Users";
import Course from "views/examples/Course";
import { v4 as uuidv4, NIL as NIL_UUID } from 'uuid';
import StudentCourse from "views/examples/StudentCourse";


var role = localStorage.getItem('role');

var sidebarroutes = []

var instructorRoutes = [
  {
    path: "/index",
    name: "Subjects",
    icon: "ni ni-tv-2 text-primary",
    component: Index,
    layout: "/admin",
  },
 
  {
    path: "/users",
    name: "Users",
    icon: "ni ni-bullet-list-67 text-red",
    component: Users,
    layout: "/admin",
  },
];

var studentRoutes = [
  {
    path: "/course/"+NIL_UUID,
    name: "Course",
    icon: "ni ni-tv-2 text-primary",
    component: Course,
    layout: "/admin",
  },
  {
    path: "/StudentCourse",
    name: "Enrolled Course",
    icon: "ni ni-bullet-list-67 text-red",
    component: StudentCourse,
    layout: "/admin",
  },
];

sidebarroutes = instructorRoutes;
if(role == 'Student'){
  sidebarroutes = studentRoutes;
}

export default sidebarroutes;
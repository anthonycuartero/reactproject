/*!

=========================================================
* Argon Dashboard React - v1.2.1
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Index from "views/Index.js";
import Profile from "views/examples/Profile.js";
import Maps from "views/examples/Maps.js";
import Register from "views/examples/Register.js";
import Login from "views/examples/Login.js";
import Icons from "views/examples/Icons.js";
import Course from "views/examples/Course";
import Module from "views/examples/Module";
import { Content } from "views/examples/Content";
import Users from "views/examples/Users.js";
import StudentCourse from "views/examples/StudentCourse";
import StudentModule from "views/examples/StudentModule";

var routes = [
  {
    path: "/index",
    name: "Subjects",
    icon: "ni ni-tv-2 text-primary",
    component: Index,
    layout: "/admin",
  },
  {
    path: "/course/:subjectId",
    name: "Course",
    icon: "ni ni-tv-2 text-primary",
    component: Course,
    layout: "/admin",
  },
  {
    path: "/module/:courseId/:subjectId",
    name: "Module",
    icon: "ni ni-tv-2 text-primary",
    component: Module,
    layout: "/admin",
  },
  {
    path: "/StudentCourse",
    name: "Enrolled Course",
    icon: "ni ni-tv-2 text-primary",
    component: StudentCourse,
    layout: "/admin",
  },
  {
    path: "/StudentModule/:courseId",
    name: "Student Module",
    icon: "ni ni-tv-2 text-primary",
    component: StudentModule,
    layout: "/admin",
  },
  {
    path: "/content/:moduleId/:courseId/:subjectId",
    name: "Content",
    icon: "ni ni-tv-2 text-primary",
    component: Content,
    layout: "/admin",
  },
  {
    path: "/users",
    name: "Users",
    icon: "ni ni-bullet-list-67 text-red",
    component: Users,
    layout: "/admin",
  },
  {
    path: "/login",
    name: "Login",
    icon: "ni ni-key-25 text-info",
    component: Login,
    layout: "/auth",
  },
  {
    path: "/register",
    name: "Register",
    icon: "ni ni-circle-08 text-pink",
    component: Register,
    layout: "/auth",
  },
];
export default routes;
